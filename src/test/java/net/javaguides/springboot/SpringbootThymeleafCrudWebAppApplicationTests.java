package net.javaguides.springboot;

import com.main.SpringBootJPAEmployeeHTML;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest(classes = SpringBootJPAEmployeeHTML.class)
class SpringbootThymeleafCrudWebAppApplicationTests {

    @Test
    void contextLoads() {
    }

}
